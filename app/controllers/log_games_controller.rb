class LogGamesController < ApplicationController
  before_action :set_log_game, only: [:show, :edit, :update, :destroy]

  def index
    @log_games = LogGame.preload(:user, :opponent).where('log_games.opponent_id = ? or log_games.user_id = ? ', current_user.id, current_user.id)

  end

  def new
    @log_game = current_user.log_games_as_user.build
    @users = User.where.not(id: current_user.id).collect { |u| [ u.full_name, u.id ] }
  end

  def edit
  end

  def create
    log_game = current_user.log_games_as_user.build(log_game_params)
    if log_game.save
      redirect_to log_games_path, notice: 'Log game was successfully created.'
    else
      render :new
    end
  end

  def update
    if @log_game.update(log_game_params)
      redirect_to @log_game, notice: 'Log game was successfully updated.'
    else
      render :edit
    end
  end

  # def destroy
  #   @log_game.destroy
  #   redirect_to log_games_url, notice: 'Log game was successfully destroyed.'
  # end

  private
    def set_log_game
      @log_game = LogGame.find(params[:id])
    end

    def log_game_params
      params.require(:log_game).permit(:first_name, :last_name, :opponent_id, :user_score, :opponent_score, :date_played)
    end
end
