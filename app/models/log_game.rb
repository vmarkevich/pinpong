class LogGame < ApplicationRecord

  belongs_to :user
  belongs_to :opponent, class_name: 'User'

  validates :opponent_id, :user_id, :user_score, :opponent_score, :date_played, presence: true

  def score(current_user)
    score = [user_score, opponent_score]
    opponent_is?(current_user) ? score.reverse.join('-') : score.join('-')
  end

  def result(current_user)
    score = [user_score, opponent_score]
    result = opponent_is?(current_user) ? score.reverse : score
    result[0] > result[1] ? 'W' : 'L'
  end

  def set_opponent(current_user)
    obj = opponent_is?(current_user) ? user : opponent
    obj.full_name
  end

  private

    def opponent_is?(user)
      opponent_id == user.id
    end
end
