class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :trackable, :validatable

  has_many :log_games_as_user, class_name: 'LogGame', foreign_key: 'user_id', dependent: :destroy
  has_many :log_games_as_opponent, class_name: 'LogGame', foreign_key: 'opponent_id', dependent: :destroy

  validates :first_name, :last_name, presence: true

  def full_name
    [first_name, last_name].join(' ')
  end
end
