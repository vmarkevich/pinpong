
10.times.each do
  full_name_arr = Faker::Name.unique.name.split(' ')
  first_name = full_name_arr[0]
  last_name = full_name_arr[1]

  unless User.where(email: "#{first_name.downcase}@hello.com").present?
    User.create!(email: "#{first_name.downcase}@hello.com", password: '123456', first_name: first_name, last_name: last_name)
    puts "-- Added #{User.count} users to your database"
  else
    puts "No one"
  end
end
