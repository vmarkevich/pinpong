class CreateLogGames < ActiveRecord::Migration[5.1]
  def change
    create_table :log_games do |t|

      t.datetime :date_played
      t.integer :opponent_id
      t.integer :user_id
      t.integer :user_score
      t.integer :opponent_score

      t.timestamps
    end
  end
end
