class AddIndexInToLogGames < ActiveRecord::Migration[5.1]
  def up
    add_index :log_games, :user_id
    add_index :log_games, :opponent_id
  end

  def down
    remove_index :log_games, :user_id
    remove_index :log_games, :opponent_id
  end
end
