Rails.application.routes.draw do
  devise_for :users

  root 'home#index'

  get '/history', to: 'home#history'

  resources :log_games, except: [:show, :destroy]
end
